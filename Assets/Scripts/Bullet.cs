using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class Bullet : NetworkBehaviour
{
    public float force = 1000;
    public float destroyAfter = 2;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * force);
    }
    public override void OnStartServer()
    {
        base.OnStartServer();



        Invoke(nameof(DestroySelf), destroyAfter);


    }

    [Server]
    void DestroySelf()
    {

        NetworkServer.Destroy(gameObject);



    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
