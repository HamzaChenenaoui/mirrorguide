using Mirror;
using Newtonsoft.Json.Bson;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerScript : NetworkBehaviour
{
    [SerializeField]
    private GameObject Lost_txt;
    [SerializeField]
    private GameObject HP_txt;
    private float fireRate = 0.5f;
    private float nextFireTime = 0;
    public TextMeshPro healthBar;
    public GameObject bulletPrefab;
    public Transform firePoint;
    [SyncVar(hook =nameof(OnHealthChangedHook))]
    public int healthCurrent;
    private Material playerMaterialClone;
    [SyncVar(hook =nameof(OnColorChange))]
    public Color playerColor = Color.white; 
    // Start is called before the first frame update
    void OnHealthChangedHook(int _old,int _new)
    {

        healthBar.text = "HP:" + _new;




    }
    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {

            HitEvent(other);
            Destroy(other.gameObject);
        }
    }
    public void HitEvent(Collider _collider) { 
    
    if (_collider.CompareTag("Bullet"))
        {
            healthCurrent -= 3;
        }
    
    }

    void OnColorChange(Color _old, Color _new)
    {
        playerMaterialClone = new Material(GetComponent<Renderer>().material);
        playerMaterialClone.color = _new;
        GetComponent<Renderer>().material = playerMaterialClone;    
    }
    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        CmdSetupColor(color);
        int health = healthCurrent;
        CmdSetupHealth(health);
    }
    [Command]
    void CmdSetupColor(Color _col)
    {

        playerColor = _col; 



    }
    [Command]
    void CmdSetupHealth(int _he)
    {

        healthCurrent = _he;
        OnHealthChangedHook(healthCurrent, _he);



    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer == true ) { 
        float moveX = Input.GetAxis("Horizontal") * Time.deltaTime * 110.0f;
        float moveZ = Input.GetAxis("Vertical") * Time.deltaTime * 4f;

        transform.Rotate(0, moveX, 0);
        transform.Translate(0, 0, moveZ);
          if(Input.GetButtonDown("Fire1")&& Time.time >= nextFireTime)
            {
                CmdShoot();
                nextFireTime = Time.time +0.5f / fireRate;   

            }
        }
        RpcGameOver();
    }
    [Command]
    void CmdShoot()
    {


        GameObject bullet = Instantiate(bulletPrefab,firePoint.position, firePoint.rotation);   
        NetworkServer.Spawn(bullet);    


    }
    [ClientRpc]
    void RpcGameOver()
    {

      if (healthCurrent <= 0)
        {
            StartCoroutine(GameOverRoutine());

        }
    }
 IEnumerator GameOverRoutine()
    {

        Lost_txt.gameObject.SetActive(true);    
        HP_txt.gameObject.SetActive(false);  
        yield return new WaitForSeconds(2.0f);  
        NetworkServer.Destroy(gameObject);    





    }
}
